<?php

namespace Isolate;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class PluginInstaller extends LibraryInstaller
{
    private static $options = [
        'bincms-app-dir' => 'app'
    ];

    /**
     * {@inheritDoc}
     */
    public function getPackageBasePath(PackageInterface $package)
    {
        $prefix = substr($package->getPrettyName(), 0, 17);

        if ('bincms/extension-' !== $prefix) {
            throw new \InvalidArgumentException(
                'Unable to install extension, BinCMS extensions '
                .'should always start their package name with '
                .'"bincms/extension-"'
            );
        }

        $options = $this->getOptions();

        return $options['bincms-app-dir'].'/extensions/'.substr($package->getPrettyName(), 17);
    }

    protected function getOptions()
    {
        return array_merge(self::$options, $this->composer->getPackage()->getExtra());
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'bincms-extension' === $packageType;
    }

}
